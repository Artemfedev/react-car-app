import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import cars from './cars';

export default combineReducers({
    routing: routerReducer,
    cars
});
