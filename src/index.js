import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { Router, Route, hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import App from './App';
import './index.css';
import reducer from './reducers';
import Car from './Car';

const store = createStore(reducer);
const history = syncHistoryWithStore(hashHistory, store);

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={App}/>
            <Route path="/cars/:id" component={Car}/>
        </Router>
    </Provider>,
    document.getElementById('root')
);
