import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import './App.css';
const App = ({ cars }) => {
    return (
        <div className="container-fluid">
            <h2>Fetched cars</h2>
            <div className="row">
                {cars.map((car, index) =>
                    <div key={index} className="col-md-4">
                        <Link to={`/cars/${car.id}`}>
                            <div className="jumbotron">
                                <img src={car.img} role="presentation"/>
                                <p>Название: {car.title}</p>
                                <p>Цена: {car.year}</p>
                                <p>Год: {car.price}</p>
                            </div>
                        </Link>
                    </div>
                )}
            </div>
        </div>
    );
};

export default connect(
    state => ({
        cars: state.cars
    })
)(App);
