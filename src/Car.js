import React from 'react';
import { connect } from 'react-redux';
import './App.css';

const Car = ({ car }) =>
    <div className="row car">
        <div className="col-6 img">
            <img src={car.img} role="presentation"/>
        </div>
        <div className="col-6 text">
            <div>Название: {car.title}</div>
            <div>Год: {car.year}</div>
            <div>Цена: {car.price}</div>
        </div>
    </div>;

const mapStateToProps = ({ cars }, ownProps) => {
    return {
        car: cars[ownProps.params.id]
    };
};

export default connect(mapStateToProps)(Car);
