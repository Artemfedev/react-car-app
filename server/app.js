import request from "request";
import cheerio from "cheerio";
import fs from 'fs';
let url = "http://rst.ua/oldcars/audi/";
let auto = [];
request(url, function (error, response, html) {

        let $ = cheerio.load(html);
        $('.rst-ocb-i-d').each(function(i) {
            let a = $(this).prev();
            let title = a.children('.rst-ocb-i-h').children().text().replace("������", "").trim();
            let img = a.children('img').attr('src');
            let price = a.next().children('ul').children('.rst-ocb-i-d-l-i').children('strong').children().text().replace("���", "").trim();
            let year = a.next().children('ul').children('.rst-ocb-i-d-l-i').children('.rst-ocb-i-d-l-i-s').eq(0).text();
            auto.push({
                id: i,
                img: img,
                title: title,
                price: price,
                year: year
            });
        });

    console.log(auto);
    let data = JSON.stringify(auto);
    fs.writeFileSync('./src/reducers/carfile.json', data);
});